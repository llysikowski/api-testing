import requests
from utils.poke_api_class import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_response_not_empty():
    """Pokemon API: assert /v2/pokemon response is not empty"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/")
    body = response.json()
    assert len(body) != 0
    assert len(body["results"]) > 0
    assert body["results"]


def test_status_code_is_200():
    """Pokemon API: assert /v2/pokemon response has status 200"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/")
    assert response.status_code == 200


def test_pokemon_count_is_1279():
    """Pokemon API: assert /v2/pokemon has 1279 items"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/")
    body = response.json()
    assert body["count"] == 1279


def test_response_time_under_1s():
    """Pokemon API: assert /v2/pokemon response time is under 1 second"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/")
    assert response.elapsed.total_seconds() < 1
    assert response.elapsed.microseconds / 1000 < 1000


def test_response_size_under_100kb():
    """Pokemon API: assert /v2/pokemon response size is under 100 kilobytes"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/")
    assert len(response.content) / 1000 < 100


def test_poke_list_offset_limit():
    """Pokemon API: assert /v2/pokemon pagination works as expected"""
    params = {
        "offset": 20,
        "limit": 10
    }
    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()
    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"
    assert len(body["results"]) == params["limit"]


def test_pokemon_shape():
    """Pokemon API: assert /v2/pokemon-shape retrieving pokemon shape works as expected"""
    shape_no = 3
    response = pokeapi_handler.get_shapes_of_pokemons()
    body = response.json()
    assert body["count"] == len(body["results"])

    third_shape_name = body["results"][shape_no-1]["name"]
    response_name = pokeapi_handler.get_shapes_of_pokemons(third_shape_name)
    body_name = response_name.json()
    assert body_name["id"] == shape_no
