from faker import Faker
import random
from utils.gorest_handler import GoRESTHandler

gorest_handler = GoRESTHandler()
fake = Faker()


def test_create_update_delete_user():
    """GoREST API: /v2/users create, update and delete user"""
    user_data = {
        "name": fake.language_name(),
        "gender": random.choice(['male', 'female']),
        "email": fake.email(),
        "status": random.choice(['active', 'inactive']),
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    assert body["id"] == user_id

    user_data["email"] = fake.email()  # changing email to update user data
    user_data["name"] = fake.language_name()  # changing name to update user data
    body = gorest_handler.update_user(user_id, user_data).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    assert body["id"] == user_id

    gorest_handler.delete_user(user_id)
    body = gorest_handler.get_user(user_id, False).json()
    assert body["message"] == "Resource not found"


def test_create_user_existing_email():
    """GoREST API: /v2/users create user with mail existing in the database"""
    user_data = {
        "name": fake.language_name(),
        "gender": random.choice(['male', 'female']),
        "email": fake.email(),
        "status": random.choice(['active', 'inactive']),
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    assert body["id"] == user_id

    body = gorest_handler.create_user(user_data, True).json()
    assert body[0]["field"] == "email"
    assert body[0]["message"] == "has already been taken"
